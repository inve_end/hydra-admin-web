import { commonService } from '@/api/common';

const state = {
    all: {},
    cards: [],
    rangeCode: [],
    companies: [],
    order: [],
    orderList: []
}

const actions = {
    getCards({commit}) {
        commit('getCardRequest');
        commonService.getCards()
        .then(
            cards => commit('getCardSuccess', cards),
            error => commit('getCardFailed', error)
        )
    },
    getRangeCode({commit}) {
        commit('getRangeCodeRequest');
        commonService.getRangeCode()
        .then(
            rangeCode => commit('getRangeCodeSuccess', rangeCode),
            error => commit('getRangeCodeFailed', error)
        )
    },
    getCompanies({commit}) {
        commit('getCompaniesRequest');
        commonService.getCompanies()
        .then(
            companies => commit('getCompaniesSuccess', companies),
            error => commit('getCompaniesFailed', error)
        )
    },
    getOrderDeposit({commit}, card) {
        commit('getOrderRequest');
        commonService.getOrderDeposit(card)
        .then(
            order => commit('getOrderSuccess', order),
            error => commit('getOrderFailed', error)
        )
    },
    getOrderWithdraw({commit}, card) {
        commit('getOrderRequest');
        commonService.getOrderWithdraw(card)
        .then(
            order => commit('getOrderSuccess', order),
            error => commit('getOrderFailed', error)
        )
    }
}

const mutations = {
    getCardRequest(state) {
        state.all = { loading: true };
    },
    getCardSuccess(state, cards) {
        state.cards = cards.data;
    },
    getCardFailed(state, error) {
        state.all = { error };
    },
    getRangeCodeRequest(state) {
        state.all = { loading: true };
    },
    getRangeCodeSuccess(state, rangeCode) {
        state.rangeCode = rangeCode.data;
    },
    getRangeCodeFailed(state, error) {
        state.all = { error };
    },
    getCompaniesRequest(state) {
        state.all = { loading: true };
    },
    getCompaniesSuccess(state, companies) {
        state.companies = companies.data;
    },
    getCompaniesFailed(state, error) {
        state.all = { error };
    },
    getOrderRequest(state) {
        state.all = { loading: true };
    },
    getOrderSuccess(state, order) {
        state.orderList.length = 0
        state.orderList = order.data
        state.order = order.data
    },
    getOrderFailed(state, error) {
        state.all = { error };
    }
}

export const common = {
    namespaced: true,
    state,
    actions,
    mutations
}
