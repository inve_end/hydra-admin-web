import { cardsService } from '@/api/cards'

const state = {
    all: {},
    dailycardstats: [],
    weeklycardstats: [],
    monthlycardstats: [],
    cardhistory: []
}

const actions = {
    getDailyCardStats({commit}, card) {
        commit('getDailyCardStatsRequest');
        cardsService.getCardStatsDaily(card)
        .then(
            card => commit('getDailyCardStatsSuccess', card),
            error => commit('getDailyCardStatsFailed', error)
        )
    },
    getWeeklyCardStats({commit}, card) {
        commit('getWeeklyCardStatsRequest');
        cardsService.getCardStatsWeekly(card)
        .then(
            card => commit('getWeeklyCardStatsSuccess', card),
            error => commit('getWeeklyCardStatsFailed', error)
        )
    },
    getMonthlyCardStats({commit}, card) {
        commit('getMonthlyCardStatsRequest');
        cardsService.getCardStatsMonthly(card)
        .then(
            card => commit('getMonthlyCardStatsSuccess', card),
            error => commit('getMonthlyCardStatsFailed', error)
        )
    },
    getCardLoginHistory({commit}, card) {
        commit('getCardLoginHistoryRequest');
        cardsService.getCardLoginHistory(card)
        .then(
            card => commit('getCardLoginHistorySuccess', card),
            error => commit('getCardLoginHistoryFailed', error)
        )
    }
}

const mutations = {
    getDailyCardStatsRequest(state) {
        state.all = { loading: true };
    },
    getDailyCardStatsSuccess(state, dailycardstats) {
        state.dailycardstats = dailycardstats.data;
    },
    getDailyCardStatsFailed(state, error) {
        state.all = { error };
    },
    getWeeklyCardStatsRequest(state) {
        state.all = { loading: true };
    },
    getWeeklyCardStatsSuccess(state, weeklycardstats) {
        state.weeklycardstats = weeklycardstats.data;
    },
    getWeeklyCardStatsFailed(state, error) {
        state.all = { error };
    },
    getMonthlyCardStatsRequest(state) {
        state.all = { loading: true };
    },
    getMonthlyCardStatsSuccess(state, monthlycardstats) {
        state.monthlycardstats = monthlycardstats.data;
    },
    getMonthlyCardStatsFailed(state, error) {
        state.all = { error };
    },
    getCardLoginHistoryRequest(state) {
        state.all = { loading: true };
    },
    getCardLoginHistorySuccess(state, cardhistory) {
        state.cardhistory = cardhistory.data;
    },
    getCardLoginHistoryFailed(state, error) {
        state.all = { error };
    }
}

export const cards = {
    namespaced: true,
    state,
    actions,
    mutations
}
