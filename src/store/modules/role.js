import { getRoleList } from '@/api/role';

const state = {
    all: {}, 
    roles: [],
    loading: true
};

const actions = {
    getAll({commit}) {
        commit('getAllRequest');
        getRoleList()
            .then(
                roles => commit('getAllSuccess', roles),
                error => commit('getAllFailure', error)
            );
    }
}


const mutations = {
    getAllRequest(state) {
        state.all = {loading: true};
    },    
    getAllSuccess(state, roles) {
        state.roles = roles ? roles.data ? roles.data : roles : roles;
        state.loading = false;
    },
    getAllFailure(state, error) {
        state.all = {error};
    }
}

const roles = {
    namespaced: true,
    state,
    actions,
    mutations
};
export default roles;
