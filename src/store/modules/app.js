import Cookies from 'js-cookie'

const app = {
  state: {
    sidebar: {
      opened: !+Cookies.get('sidebarStatus')
    },
    listReq: {
      $size: 30,
      $page: 1
    },
    pageSizeArr: [5, 10, 30, 100, 200],
    statusArray: ['可用','禁用'],
    switchArray: ['关闭','开启'],
    ReChargeStatus: ['创建','提交', '成功', '撤销'],
    BankCardType: { DEPOSIT: 0, WITHDRAW: 1 }
  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
    },
    SET_Language (state, data) {
      state.Language = data
    }
  },
  actions: {
    ToggleSideBar: ({commit}) => {
      commit('TOGGLE_SIDEBAR')
    },
    setLanguage: ({commit,state}, play) => {
      commit('SET_Language', play)
    }
  }
}

export default app
