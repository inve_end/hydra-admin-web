import { login, logout, getInfo, signIn, userSet } from '@/api/login'
import { getToken, setToken, removeToken, setUserId, getUserId } from '@/utils/auth'
import { getRoleList } from '@/api/role';

const user = {
  state: {
    token: getToken(),
    name: '',
    roles: null,
    perm: '',
    id: getUserId(),
    created_at: '',
    updated_at: '',
    user_roles: {},
    lang: [],
    user_perms: null
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_PERMS: (state, perm) => {
      state.user_perms = perm
    },
    SET_PERM: (state, perm) => {
      state.perm = perm
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_USERID: (state, id) => {
      state.id = id
    },
    SET_CREATEDTIME: (state, created_at) => {
      state.created_at = created_at
    },
    SET_USERROLES: (state, roles) => {
      state.user_roles = roles;
    },
    SET_UPDATETIME: (state, updated_at) => {
      state.updated_at = updated_at
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.nickname
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          if(response.status === 0){
            const data = response.data
            setToken(data.token)
            setUserId(data.id)
            commit('SET_TOKEN', data.token)
            commit('SET_USERID', data.id)
            getRoleList().then(res => {
              if (res.status === 0) {
                let roleData = {};
                res.data.list.map(item => {
                  roleData[item.id] = item.rolename;
                });
                commit('SET_USERROLES', roleData);
              }
            })
            resolve()
          } else {
            this.$message.error(response.msg)
          }
        }).catch(error => {
          reject(error)
        })
      });     
    },

    // 注册
    Register({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        signIn(userInfo).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 个人设置
    UserSet({ commit, state }, userInfo) {
      return new Promise((resolve, reject) => {
        userSet(userInfo, state.id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.id).then(response => {
          const data = response.data

          commit('SET_PERMS', data.perm);
          commit('SET_ROLES', data.role)
          commit('SET_NAME', data.nickname)
          commit('SET_PERM', '|'.concat(data.perm))
          commit('SET_UPDATETIME', data.updated_at)
          commit('SET_CREATEDTIME', data.created_at)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          console.log('dd')
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
