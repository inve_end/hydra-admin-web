import { rangeService } from '@/api/range'

const state = {
    all: {},
    range: [],
    range_collection: [],
    range_payment: []
}

const actions = {
    getRangeList({commit}, rangedata) {
        commit('getRangeRequest');
        rangeService.getRangeList(rangedata)
        .then(
            range => commit('getRangeSuccess', range),
            error => commit('getRangeFailed', error)
        )
    },
    getRangeCollectionList({commit}, rangedata) {
      commit('getRangeRequest');
      rangeService.getRangeList(rangedata)
      .then(
          range => commit('getRangeCollectionChangeSuccess', range),
          error => commit('getRangeFailed', error)
      )
    },
    getRangePaymentList({commit}, rangedata) {
      commit('getRangeRequest');
      rangeService.getRangeList(rangedata)
      .then(
          range => commit('getRangePaymentChangeSuccess', range),
          error => commit('getRangeFailed', error)
      )
    },
    insertRange({commit}, rangedata) {
        //console.log(rangedata.param)
        rangeService.insertRange( rangedata )
          .then( value => {
            return value;
          } )
          .finally( function () {
            commit( 'getRangeRequest');
            rangeService
              .getRangeList(rangedata.param)
              .then(
                range => commit(rangedata.state_func, range),
                error => commit( 'getRangeFailed', error )
              );
          } )
          .catch( err => {
            console.log( err );
          } )
    },
    updatetRange({commit}, rangedata) {
        rangeService.updatetRange( rangedata )
          .then( value => {
            return value;
          } )
          .finally( function () {
            commit( 'getRangeRequest' );
            rangeService
              .getRangeList(rangedata.param)
              .then(
                range => commit(rangedata.state_func, range),
                error => commit( 'getRangeFailed', error )
              );
          } )
          .catch( err => {
            console.log( err );
          } )
    },
    updatetRangeStatus({commit}, rangedata) {
        rangeService.updatetRangeStatus( rangedata )
          .then( value => {
            return value;
          } )
          .finally( function () {
            commit( 'getRangeRequest' );
            rangeService
              .getRangeList(rangedata.param)
              .then(
                range => commit(rangedata.state_func, range),
                error => commit( 'getRangeFailed', error )
              );
          } )
          .catch( err => {
            console.log( err );
          } )
    },
    assignRange({commit}, rangedata) {
        rangeService.assignRange( rangedata )
          .then( value => {
            return value;
          } )
          .finally( function () {
            commit( 'getRangeRequest' );
            rangeService
              .getRangeList(rangedata.param)
              .then(
                range => commit(rangedata.state_func, range ),
                error => commit( 'getRangeFailed', error )
              );
          } )
          .catch( err => {
            console.log( err );
          } )
    }
}

const mutations = {
    getRangeRequest(state) {
        state.all = { loading: true };
    },
    getRangeSuccess(state, range) {
        state.range = range.data;
    },
    getRangeFailed(state, error) {
        state.all = { error };
    },
    getAddRangeSuccess(state, range) {
        state.range = range.data;
    },
    getRangeChangeSuccess(state, range) {
        state.range = range.range.data;
    },
    getRangeCollectionChangeSuccess(state, collection) {
      state.range_collection = collection.data;
    },
    getRangePaymentChangeSuccess(state, payment) {
      state.range_payment = payment.data;
    }
}

export const range = {
    namespaced: true,
    state,
    actions,
    mutations
}
