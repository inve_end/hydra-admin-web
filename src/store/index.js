import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import getters from './getters'
import { common } from './modules/common'
import { range } from './modules/range'
import { cards } from './modules/cards'
import roles from './modules/role';
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user,
    common,
    range,
    cards,
    roles
  },
  getters
})

export default store
