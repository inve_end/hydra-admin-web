const getters = {
  sidebar: state => state.app.sidebar,
  token: state => state.user.token,
  nickname: state => state.user.nickname,
  name: state => state.user.name,
  roles: state => state.user.roles
}
export default getters
