import { enLogin } from '@/views/login/index';
import { enOrderWithdraw } from '@/views/orderWithdraw/index';
import { enOrderModal } from '@/views/orderWithdraw/orderModal';
import { enOrderBatchModal } from '@/views/orderWithdraw/orderBatchModal';
import { enMerchant } from '@/views/merchant/index';
import { enSaveMerchant } from '@/views/merchant/saveMch';
import { enOrder } from '@/views/order/index';
import { enRouter } from '@/router/index';
import { enExemgt } from '@/views/exemgt/packagelist';
import { enRightAccess } from '@/views/rightAccess/index';
import { enRole } from '@/views/role/index';
import { enRange } from '@/views/range/collection';
import { enRangeAdd } from '@/views/range/addRange';
import { enRangeEdit } from '@/views/range/editRange';
import { enRangeStatus } from '@/views/range/statusRange';
import { enRangeAssign } from '@/views/range/assignRange';
import { enOperatorLog } from '@/views/operatLog/operatLog';
import { enTransactionPmt } from '@/views/transaction/payment/index';
import { enTransactionCol } from '@/views/transaction/collection/index';
import { enCards } from '@/views/bankcard/collection';
import { enSms } from '@/views/sms/index';
import { enTransModalDtl } from '@/views/transaction/collection/showDetail';
import { enTransMatchOrder } from '@/views/transaction/collection/matchOrder';
import { enTransUser } from '@/views/userManage/index';
import { enTransUserEdit } from '@/views/userManage/updateUser';
import { enPersonalSettings } from '@/views/layout/components/UserSet';
import { enUpdateRole } from '@/views/role/updateRole';

export default {
    router: enRouter,
    login: enLogin,
    exemgt: enExemgt,
    cards: enCards,
    sms: enSms,
    userset: enPersonalSettings,
    // Order Withdraw
    orderWithdraw: enOrderWithdraw,
    orderModal: enOrderModal,
    orderBatchModal: enOrderBatchModal,

    // Order Deposit
    order: enOrder,

    // Merchant
    merchant: enMerchant,
    saveMerchant: enSaveMerchant,

    // Right Access
    rightAccess: enRightAccess,
    role: enRole,
    updateRole: enUpdateRole,

    //Range
    range: enRange,
    rangeAdd: enRangeAdd,
    rangeEdit: enRangeEdit,
    rangeStatus: enRangeStatus,
    rangeAssign: enRangeAssign,

    //Operator Log
    operatorLog: enOperatorLog,

    
    //Transaction mgt Payment/Collection
    pmTransaction: enTransactionPmt,
    colTransaction: enTransactionCol,
    colModalTransDtl: enTransModalDtl,    
    colModalMatchOrder: enTransMatchOrder,
    userTranslate: enTransUser,
    updateUserTranslate: enTransUserEdit
}
