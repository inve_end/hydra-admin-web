import { zhLogin } from '@/views/login/index';
import { zhOrderWithdraw } from '@/views/orderWithdraw/index';
import { zhOrderModal } from '@/views/orderWithdraw/orderModal';
import { zhOrderBatchModal } from '@/views/orderWithdraw/orderBatchModal';
import { zhMerchant } from '@/views/merchant/index';
import { zhSaveMerchant } from '@/views/merchant/saveMch';
import { zhOrder } from '@/views/order/index';
import { zhRouter } from '@/router/index';
import { zhExemgt } from '@/views/exemgt/packagelist';
import { zhRightAccess } from '@/views/rightAccess/index';
import { zhRole } from '@/views/role/index';
import { zhRange } from '@/views/range/collection';
import { zhRangeAdd } from '@/views/range/addRange';
import { zhRangeEdit } from '@/views/range/editRange';
import { zhRangeStatus } from '@/views/range/statusRange';
import { zhRangeAssign } from '@/views/range/assignRange';
import { zhOperatorLog } from '@/views/operatLog/operatLog';
import { zhCards } from '@/views/bankcard/collection';
import { zhSms } from '@/views/sms/index';
import { zhTransactionCol } from '@/views/transaction/collection/index';
import { zhTransactionPmt } from '@/views/transaction/payment/index';
import { zhTransModalDtl } from '@/views/transaction/collection/showDetail';
import { zhTransMatchOrder } from '@/views/transaction/collection/matchOrder';
import { zhTransUser } from '@/views/userManage/index';
import { zhTransUserEdit } from '@/views/userManage/updateUser';
import { zhUpdateRole } from '@/views/role/updateRole';
import { zhPersonalSettings } from '@/views/layout/components/UserSet';

export default {
    router: zhRouter,
    login: zhLogin,
    exemgt: zhExemgt,
    cards: zhCards,
    sms: zhSms,
    userset: zhPersonalSettings,
    // Order Withdraw
    orderWithdraw: zhOrderWithdraw,
    orderModal: zhOrderModal,
    orderBatchModal: zhOrderBatchModal,

    // Order Deposit
    order: zhOrder,

    // Merchant
    merchant: zhMerchant,
    saveMerchant: zhSaveMerchant,

    // Right Access
    rightAccess: zhRightAccess,
    role: zhRole,
    updateRole: zhUpdateRole,
    
    //Range
    range: zhRange,
    rangeAdd: zhRangeAdd,
    rangeEdit: zhRangeEdit,
    rangeStatus: zhRangeStatus,
    rangeAssign: zhRangeAssign,

    //Operator Log
    operatorLog: zhOperatorLog,
    
    //trasactions
    pmTransaction: zhTransactionPmt,
    colTransaction: zhTransactionCol,
    colModalTransDtl: zhTransModalDtl,
    colModalMatchOrder: zhTransMatchOrder,
    userTranslate: zhTransUser,
    updateUserTranslate: zhTransUserEdit
}
