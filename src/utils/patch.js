
export function comparativeData(newdata, exdata){
  // console.log(newdata.name)
  // console.log(exdata)
  let olddata = JSON.parse(exdata)
  let data = {}
  let keys = Object.keys(newdata)
  if(!keys.length){
    return
  }
  keys.forEach(key => {
    if(newdata[key] && olddata[key]){
      if(newdata[key].toString() !== olddata[key].toString()){
        data[key] = newdata[key]
      }
    } else if(!newdata[key] && !olddata[key]){
    } else {
      data[key] = newdata[key]
    }
  })
  return data
}

export function queryFilter(queryObj){
  let keysP = Object.keys(queryObj)
  keysP.map(key => {
    if(queryObj[key] === ''){
      delete queryObj[key]
    }
  })
  return queryObj
}

export function encodeQueryData(data) {
  const ret = [];
  for(let d in data) {
      ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
  }
  return ret.join('&');
}
