import Cookies from 'js-cookie'

const TokenKey = 'pay_user_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  // document.addCookie('pay_user_token=' + token)
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setUserId(id) {
  // document.addCookie('pay_user_id=' + id)
  return Cookies.set('hydra_user_id', id)
}

export function getUserId() {
  return Cookies.get('hydra_user_id')
}

export function removeUserId() {
  return Cookies.remove('hydra_user_id')
}
