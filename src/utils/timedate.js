const dateFormat = require('dateformat')
const defaultTime = new Date()
const dateToday = dateFormat(defaultTime, 'yyyy-mm-dd 00:00:00')
const dateYesterday = dateFormat(new Date(Date.parse(defaultTime) - 1000 * 60 * 60 * 24), 'yyyy-mm-dd 00:00:00')
const dateThreeday = dateFormat(new Date(Date.parse(defaultTime) - 1000 * 60 * 60 * 24 * 3), 'yyyy-mm-dd 00:00:00')
const dateWeek = dateFormat(new Date(Date.parse(defaultTime) - 1000 * 60 * 60 * 24 * 7), 'yyyy-mm-dd 00:00:00')

export default {
    dateToday,
    dateYesterday,
    dateThreeday,
    dateWeek
}
