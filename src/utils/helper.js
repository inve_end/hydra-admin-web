export function find(obj, key, value) {
	value = typeof value === 'boolean' ? value.toString() : value;
	let result = []
	try {
		if (obj != null) {
			for (let prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					if (obj[prop][key] != null) {
						let item = typeof obj[prop][key] === 'boolean' ? obj[prop][key].toString().toLowerCase() : obj[prop][key].toString().toLowerCase();
						let n = item.search(value.toLowerCase());
						if (n >= 0) {
							result.push(obj[prop]);
						}
					}
				}
			}
		}
		return result;
	} catch (e) {
		console.log(e);
	}
}

export function finditem(obj, key, value) {
	value = typeof value === 'boolean' ? value.toString() : value;
	let result = []
	try {
		if (obj != null) {
			for (let prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					if (obj[prop][key] != null) {
						let item = typeof obj[prop][key] === 'boolean' ? obj[prop][key].toString() : obj[prop][key].toString();
						let n = item.search(value);
						if (n >= 0) {
							result.push(obj[prop]);
						}
					}
				}
			}
		}
		return result;
	} catch (e) {
		console.log(e);
	}
}

export function findstrict(obj, key, value) {
	value = typeof value === 'boolean' ? value.toString() : value;
	let result = [];
	let n = [];
	try {
		if (obj != null) {
			for (let prop in obj) {
				if (obj.hasOwnProperty(prop)) {
					if (obj[prop][key] != null) {
						if(obj[prop][key] !== value){
							continue;
						}
						let item = typeof obj[prop][key] === 'boolean' ? obj[prop][key].toString().toLowerCase() : obj[prop][key].toString().toLowerCase();
						if(typeof value === 'string'){
							n = item.search(value.toLowerCase());
						}else{
							n = item.search(value);
						}
						if (n >= 0) {
							result.push(obj[prop]);
						}
					}
				}
			}
		}
		return result;
	} catch (e) {
		console.log(e);
	}
}
