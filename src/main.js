import Vue from 'vue'
import i18n from './lang'

import 'normalize.css/normalize.css'// A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss' // global css

import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon
import '@/assets/icon/iconfont.css'
import '@/permission' // permission control

Vue.use(ElementUI, {i18n: (key, value) => i18n.t(key, value)})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  i18n,
  router,
  store,
  template: '<App/>',
  components: { App }
})
