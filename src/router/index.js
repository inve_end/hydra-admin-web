import Vue from 'vue'
import Router from 'vue-router'

/* Layout */
import Layout from '../views/layout/Layout'
// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)


/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
export const constantRouterMap = [
    {path: '/login', component: () => import('@/views/login/index'), hidden: true},
    {path: '/404', component: () => import('@/views/404'), hidden: true},

    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        name: 'Dashboard',
        hidden: true,
        children: [{
            path: 'dashboard',
            component: () => import('@/views/dashboard/index')
        }]
    },
    {
        path: '/operation',
        component: Layout,
        redirect: '/operation/merchant',
        name: 'operation',
        meta: {title: '运营相关', icon: 'process', prem: [301, 401]},
        children: [
            {
                path: 'merchant',
                name: 'merchant',
                component: () => import('@/views/merchant/index'),
                meta: {title: '商户管理', icon: 'yonghuguanli', prem: '301'}
            },
            {
                path: 'sms',
                name: 'sms',
                component: () => import('@/views/sms/index'),
                meta: {title: '短信记录', icon: 'liushuizhangdan-xiugai', prem: '401'}
            }
        ]
    },
    {
        path: '/bankcard',
        component: Layout,
        redirect: '/bankcard/collection',
        name: 'bankcard',
        meta: {title: '卡片管理', icon: 'process', prem: [501, 601, 611]},
        children: [
            {
                path: 'collection',
                name: 'collection',
                component: () => import('@/views/bankcard/collection'),
                meta: {title: '采集', icon: 'liushuizhangdan-xiugai', prem: '501'}
            }, {
                path: 'payment',
                name: 'payment',
                component: () => import('@/views/bankcard/payment'),
                meta: {title: '付款', icon: 'liushuizhangdan-xiugai', prem: '601'}
            }, {
                path: 'buffer',
                name: 'buffer',
                component: () => import('@/views/bankcard/buffer'),
                meta: {title: '中转', icon: 'liushuizhangdan-xiugai', prem: '611'}
            }
        ]
    },
    {
        path: '/range',
        component: Layout,
        redirect: '/range/collection',
        name: 'range',
        meta: {title: '范围', icon: 'process', prem: [701, 801]},
        children: [
            {
                path: 'collection',
                name: 'range_collection',
                component: () => import('@/views/range/collection'),
                meta: {title: '充值', icon: 'liushuizhangdan-xiugai', prem: '701'}
            }, {
                path: 'payment',
                name: 'range_payment',
                component: () => import('@/views/range/payment'),
                meta: {title: '付款', icon: 'liushuizhangdan-xiugai', prem: '801'}
            }
        ]
    },
    {
        path: '/order',
        component: Layout,
        redirect: '/order/recharge',
        name: 'order',
        meta: {title: '订单管理', icon: 'process', prem: [901, 1001, 1011, 1021]},
        children: [
            {
                path: 'recharge',
                name: 'recharge',
                component: () => import('@/views/order/index'),
                meta: {title: 'recharge', icon: 'liushuizhangdan-xiugai', prem: '901'}
            }, {
                path: 'orderwithdraw',
                name: 'orderwithdraw',
                component: () => import('@/views/orderWithdraw/index'),
                meta: {title: 'withdraw', icon: 'liushuizhangdan-xiugai', prem: '1001'}
            }, {
                path: 'bufferrecharge',
                name: 'bufferrecharge',
                component: () => import('@/views/bufferRecharge/index'),
                meta: {title: 'bufferrecharge', icon: 'liushuizhangdan-xiugai', prem: '1011'}
            }, {
                path: 'bufferwithdraw',
                name: 'bufferwithdraw',
                component: () => import('@/views/bufferWithdraw/index'),
                meta: {title: 'bufferwithdraw', icon: 'liushuizhangdan-xiugai', prem: '1021'}
            }
        ]
    },
    {
        path: '/transaction',
        component: Layout,
        redirect: 'bankrecord',
        name: 'Transaction',
        meta: {title: '银行卡记录', icon: 'jiaoyixianxing', prem: [1101, 1201]},
        children: [
            {
                path: 'collection',
                name: 'Collection',
                component: () => import('@/views/transaction/collection/index'),
                meta: {title: 'recharge', icon: 'zhanghuchongzhi-xiugai', prem: '1101'}
            }, {
                path: 'payment',
                name: 'Payment',
                component: () => import('@/views/transaction/payment/index'),
                meta: {title: 'withdraw', icon: 'feiyongguanli-xiugai', prem: '1201'}
            }, {
                path: 'collectionStats',
                name: 'collectionStats',
                component: () => import('@/views/bankcard/collectionStats'),
                meta: {title: '收款汇总', icon: 'liushuizhangdan-xiugai', prem: '1101'}
            }, {
                path: 'paymentStats',
                name: 'paymentStats',
                component: () => import('@/views/bankcard/paymentStats'),
                meta: {title: '付款汇总', icon: 'liushuizhangdan-xiugai', prem: '1201'}
            }
        ]
    },
    {
        path: '/console',
        component: Layout,
        redirect: '/console/user',
        name: 'console',
        meta: {title: '控制台管理', icon: 'weixiuoff', prem: [103, 1301, 1401, 1501]},
        children: [
            {
                path: 'user',
                name: 'userMa',
                component: () => import('@/views/userManage/index'),
                meta: {title: '用户管理', icon: 'addressbook', prem: '103'}
            }, {
                path: 'role',
                name: 'role',
                component: () => import('@/views/role/index'),
                meta: {title: '角色管理', icon: 'zhangdanmingxi-xiugai', prem: '1301'}
            }, {
                path: 'rightAccess',
                name: 'rightAccess',
                component: () => import('@/views/rightAccess/index'),
                meta: {title: '权限管理', icon: 'anquanshezhi-xiugai', prem: '1401'}
            }, {
                path: 'operat',
                name: 'operatLog',
                component: () => import('@/views/operatLog/operatLog'),
                meta: {title: '操作记录', icon: 'zhangdanmingxi-xiugai', prem: '1501'}
            }
        ]
    },
    {
        path: '/exemgt',
        component: Layout,
        redirect: '/exemgt/packagelist',
        name: 'exemgt',
        meta: {title: '执行管理', icon: 'weixiuoff', prem: [1601]},
        children: [
            {
                path: 'packagelist',
                name: 'exemgt',
                component: () => import('@/views/exemgt/packagelist'),
                meta: {title: 'EXE包列表', icon: 'addressbook', prem: '1601'}
            }, {
                path: 'test',
                name: 'test',
                meta: {title: 'test', icon: 'zhangdanmingxi-xiugai', prem: '1203'}
            }
        ]
    },

    {path: '*', redirect: '/404', hidden: true}
]

export default new Router({
    // mode: 'history', //后端支持可开
    scrollBehavior: () => ({y: 0}),
    routes: constantRouterMap
})

export const enRouter = {
    运营相关: 'Operation MGT',
    卡片管理: 'Cards MGT',
    订单管理: 'Order MGT',
    银行卡记录: 'Transaction MGT',
    控制台管理: 'System MGT',
    执行管理: 'EXE MGT',
    商户管理: 'Company MGT',
    短信记录: 'SMS Record',
    范围: 'Range',
    采集: 'Collection',
    付款: 'Payment',
    中转: 'Buffer',
    收款汇总: 'CollectionStats',
    付款汇总: 'PaymentStats',
    中转汇总: 'BufferStats',
    上分订单: 'Collection',
    充值: 'Collection',
    用户管理: 'User MGT',
    EXE包列表: 'EXE List',
    操作记录: 'Operation MGT',
    角色管理: 'Role List',
    权限管理: 'Access Permission',
    test: 'test',
    personalsettings: 'Personal Settings',
    logout: 'Logout',
    welcome: 'Welcome',
    recharge: 'Collection',
    withdraw: 'Payment',
    bufferrecharge: 'Buffer Recharge',
    bufferwithdraw: 'Buffer Withdraw',
    verification_failed: 'Verification failed, please log in again',
    token_expired: 'The token expires, please log in again!'
}

export const zhRouter = {
    运营相关: '运营相关',
    卡片管理: '卡片管理',
    订单管理: '订单管理',
    银行卡记录: '银行交易记录',
    控制台管理: '控制台管理',
    执行管理: '执行管理',
    商户管理: '商户管理',
    短信记录: '短信记录',
    范围: '范围',
    采集: '收款',
    付款: '付款',
    中转: '中转',
    收款汇总: '收款汇总',
    付款汇总: '付款汇总',
    中转汇总: '中转汇总',
    上分订单: '上分订单',
    充值: '收款',
    用户管理: '用户管理',
    EXE包列表: 'EXE包列表',
    操作记录: '操作记录',
    角色管理: '角色管理',
    权限管理: '权限管理',
    test: 'test',
    personalsettings: '个人设置',
    logout: '退出登录',
    welcome: '欢迎',
    recharge: '充值',
    withdraw: '提款',
    bufferrecharge: '中转充值',
    bufferwithdraw: '中转提款',
    verification_failed: '验证失败,请重新登录',
    token_expired: 'token过期，请重新登陆!'
}
