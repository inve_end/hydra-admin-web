import request from '@/utils/request'

export function getBank(param) {
  return request({
    url: `/admin/exemgt/banks/?id=${param}`,
    method: 'get'
  })
}

export function getExemgt(param) {
    return request({
      url: `/admin/exemgt/?id=${param}`,
      method: 'get'
    })
  }

  export function uploadExe(param){
    return request({
        url: `/api/exemgt/upload_exe`,
        method: `post`,
        headers: {'Content-Type': 'multipart/form-data', 'Content-Encoding': 'gzip, deflate'},
        data: param
    })
}

export function updateExe(id, user_id){
  var body = {'id': id, 'user_id': user_id}
  return request({
      url: `/api/exemgt/update_status`,
      method: `post`,
      data: JSON.stringify(body)
  })
}

export function deleteExe(id){
  return request({
      url: `/admin/exemgt/?id=${id}`,
      method: `delete`
  })
}
