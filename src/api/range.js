import request from '@/utils/request'

export const rangeService = {
    getRangeList,
    insertRange,
    updatetRange,
    updatetRangeStatus,
    assignRange
}

function getRangeList(data) {
    return request({
      url: `/admin/range/?${data}`,
      method: 'get'
    })
}

function insertRange(data) {
    return request({
      url: `/admin/add_range`,
      method: 'post', 
      data
    })
}

function updatetRange(data) {
    return request({
      url: `/admin/edit_range`,
      method: 'post', 
      data
    })
}

function updatetRangeStatus(data) {
    return request({
      url: `/admin/edit_status_range`,
      method: 'post', 
      data
    })
}

function assignRange(data) {
    return request({
      url: `/admin/assign_cards_companies`,
      method: 'post', 
      data
    })
}
