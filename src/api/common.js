import request from '@/utils/request'

export const commonService = {
    getCards,
    getRangeCode,
    getCompanies,
    getOrderDeposit,
    getOrderWithdraw
    // get_banks,
    
}

function getCards() {
    return request({
      url: `/admin/cards`,
      method: 'get'
    })
  }

  function getRangeCode() {
    return request({
      url: `/admin/range_code`,
      method: 'get'
    })
  }

  function getCompanies() {
    return request({
      url: `/admin/company`,
      method: 'get'
    })
  }

  function getOrderDeposit(card) {
    return request({
      url: `/admin/get_order_deposit_list/?${card}`,
      method: 'get'
    })
  }

  function getOrderWithdraw(card) {
    return request({
      url: `/admin/get_order_withdraw_list/?${card}`,
      method: 'get'
    })
  }
