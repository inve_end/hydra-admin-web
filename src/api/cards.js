import request from '@/utils/request'

export const cardsService = {
    getCardStatsDaily,
    getCardStatsWeekly,
    getCardStatsMonthly,
    getCardLoginHistory
}

export function getCardStatsDaily(card) {
    return request({
      url: `api/get_card_stats_daily?card_code=${card}`,
      method: 'get'
    })
}

export function getCardStatsWeekly(card) {
    return request({
      url: `api/get_card_stats_weekly?card_code=${card}`,
      method: 'get'
    })
}

export function getCardStatsMonthly(card) {
    return request({
      url: `api/get_card_stats_monthly?card_code=${card}`,
      method: 'get'
    })
}

export function getCardLoginHistory(params){
    return request({
        url: `api/get_card_login_history_list`,
        method: 'get',
        params
    })
}
