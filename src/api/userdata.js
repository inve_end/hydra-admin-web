import request from '@/utils/request'

export function getUserList(params) {
  return request({
    url: '/admin/user/',
    method: 'get',
    params
  })
}

export function getUserDetail(id) {
  return request({
    url: `/admin/user/${id}/`,
    method: 'get'
  })
}

export function saveUserInfo(data, id) {
  return request({
    url: `/admin/user/${id}/`,
    method: 'put',
    data
  })
}

export function deleteUser(id) {
  return request({
    url: `/admin/user/${id}/`,
    method: 'delete'
  })
}


export function getPermList(params) {
  return request({
    url: '/admin/permission/',
    method: 'get',
    params
  })
}

export function saveUserPerm(data, id) {
  return request({
    url: `/admin/permission/${id}`,
    method: 'put',
    data
  })
}

export function getRecordList(params) {
  return request({
    url: '/admin/record/',
    method: 'get',
    params
  })
}
