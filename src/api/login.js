import request from '@/utils/request'

export function login(nickname, password) {
  return request({
    url: '/admin/user/login/',
    method: 'post',
    data: {
      nickname,
      password
    }
  })
}

export function getInfo(id) {
  return request({
    url: `/admin/user/${id}/`,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/admin/user/logout/',
    method: 'post'
  })
}

export function signIn(data) {
  return request({
    url: '/admin/user/',
    method: 'post',
    data: data
  })
}

export function userSet(data, id) {
  return request({
    url: `/admin/user/${id}/`,
    method: 'put',
    data: data
  })
}
