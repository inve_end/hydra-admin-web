import request from '@/utils/request'

export function getMchList(params) {
  return request({
    url: '/admin/mch/',
    method: 'get',
    params
  })
}

export function getMchDetail(id) {
  return request({
    url: `/admin/mch/${id}/`,
    method: 'get'
  })
}

export function saveMerchant(data) {
  return request({
    url: '/admin/mch/',
    method: 'post',
    data
  })
}

export function updateMerchant(data, id) {
  return request({
    url: `/admin/mch/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteMerchant(id) {
  return request({
    url: `/admin/mch/${id}`,
    method: 'delete'
  })
}

export function updateMerchantStatus(data) {
  return request({
    url: `/admin/update_mch_status`,
    method: 'post',
    data
  })
}

export function getOrderpayList(params) {
  return request({
    url: '/admin/order/',
    method: 'get',
    params
  })
}

export function OrderFresh(data) {
  return request({
    url: `/admin/order_fresh/`,
    method: 'post',
    data
  })
}

export function getAgencyList(params) {
  return request({
    url: '/admin/hydra/',
    method: 'get',
    params
  })
}

export function getAgencyDetail(id) {
  return request({
    url: `/admin/hydra/${id}/`,
    method: 'get'
  })
}

export function saveAgency(data) {
  return request({
    url: '/admin/hydra/',
    method: 'post',
    data
  })
}

export function updateAgency(data, id) {
  return request({
    url: `/admin/hydra/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteAgency(id) {
  return request({
    url: `/admin/hydra/${id}`,
    method: 'delete'
  })
}

export function resetAgencyPassword(data) {
  return request({
    url: '/admin/hydra/reset_password/',
    method: 'post',
    data
  })
}

export function createAdminRechargeOrder(data) {
  return request({
    url: '/admin/mch/create_admin_recharge_order/',
    method: 'post',
    data
  })
}

export function getAdminRechargeList(params) {
  return request({
    url: '/admin/mch/get_admin_recharge_order/',
    method: 'get',
    params
  })
}

export function lockAdminRechargeOrder(data) {
  return request({
    url: '/admin/mch/lock_admin_recharge_order/',
    method: 'post',
    data
  })
}

export function refuseAdminRechargeOrder(data) {
  return request({
    url: '/admin/mch/refuse_admin_recharge_order/',
    method: 'post',
    data
  })
}

export function make_success_admin_recharge_order(data) {
  return request({
    url: '/admin/mch/make_success_admin_recharge_order/',
    method: 'post',
    data
  })
}

export function get_hydra_money_back(data) {
  return request({
    url: '/admin/hydra/get_hydra_money_back/',
    method: 'post',
    data
  })
}

export function lockAgencyRechargeOrder(data) {
  return request({
    url: '/admin/hydra/lock_hydra_recharge_order/',
    method: 'post',
    data
  })
}

export function refuseAgencyRechargeOrder(data) {
  return request({
    url: '/admin/hydra/refuse_hydra_recharge_order/',
    method: 'post',
    data
  })
}

export function make_success_hydra_recharge_order(data) {
  return request({
    url: '/admin/hydra/make_success_hydra_recharge_order/',
    method: 'post',
    data
  })
}

export function getAgencyRechargeList(params) {
  return request({
    url: '/admin/hydra/get_hydra_recharge_order/',
    method: 'get',
    params
  })
}

export function getPlayerOrderList(params) {
  return request({
    url: '/admin/hydra/get_player_order/',
    method: 'get',
    params
  })
}

export function submit_player_order(data) {
  return request({
    url: '/admin/submit_player_order/',
    method: 'post',
    data
  })
}

export function refuse_player_order(data) {
  return request({
    url: '/admin/refuse_player_order/',
    method: 'post',
    data
  })
}

export function exportPlayerOrderList(params) {
  return request({
    url: '/admin/hydra/export_player_orders/',
    method: 'get',
    params
  })
}

export function exportAgencyOrderList(params) {
  return request({
    url: '/admin/export_hydra_orders/',
    method: 'get',
    params
  })
}

export function getAgencyReportList(params) {
  return request({
    url: '/admin/getAgencyReportList/',
    method: 'get',
    params
  })
}

export function exportAgencyReport(params) {
  return request({
    url: '/admin/export_hydra_report/',
    method: 'get',
    params
  })
}

export function chasePlayerOrder(data) {
  return request({
    url: '/admin/admin_chase_coin_from_player/',
    method: 'post',
    data
  })
}

export function getDispalyAgencyList(params) {
  return request({
    url: '/admin/get_display_hydra/',
    method: 'get',
    params
  })
}

export function admin_one_key_modify_hydra(data) {
  return request({
    url: '/admin/admin_one_key_modify_hydra/',
    method: 'post',
    data
  })
}

export function setAgencyTopShow(data) {
  return request({
    url: '/admin/set_hydra_top_show/',
    method: 'post',
    data
  })
}

export function cancelAgencyTopShow(data) {
  return request({
    url: '/admin/cancel_hydra_top_show/',
    method: 'post',
    data
  })
}

export function setAgencyShowIndex(data) {
  return request({
    url: '/admin/set_hydra_show_index/',
    method: 'post',
    data
  })
}

export function updateDisplayAgency(data) {
  return request({
    url: '/admin/update_display_hydra/',
    method: 'post',
    data
  })
}

export function setAgencyRest(data) {
  return request({
    url: '/admin/set_hydra_rest/',
    method: 'post',
    data
  })
}

export function getWorkingNotDispalyAgencyList(params) {
  return request({
    url: '/admin/get_working_not_display_hydra/',
    method: 'get',
    params
  })
}


export function set_hydra_not_online(data) {
  return request({
    url: '/admin/set_hydra_not_online/',
    method: 'post',
    data
  })
}

export function getQiniuUploadToken(data) {
  return request({
    url: '/admin/get_qiniu_upload_token/',
    method: 'post',
    data
  })
}

export function getBankcardList(params) {
  return request({
    url: '/admin/bankcard/',
    method: 'get',
    params
  })
}

export function getBankcardDetail(id) {
  return request({
    url: `/admin/bankcard/${id}/`,
    method: 'get'
  })
}

export function saveBankcard(data) {
  return request({
    url: '/admin/bankcard/',
    method: 'post',
    data
  })
}

export function updateBankcard(data, id) {
  return request({
    url: `/admin/bankcard/${id}/`,
    method: 'patch',
    data
  })
}

export function deleteBankcard(id) {
  return request({
    url: `/admin/bankcard/${id}`,
    method: 'delete'
  })
}

export function getBankcardRechargeUrl(data) {
  return request({
    url: '/admin/get_bankcard_recharge_url/',
    method: 'post',
    data
  })
}

export function getOrderList(params) {
  return request({
    url: '/admin/order/',
    method: 'get',
    params
  })
}

export function getBankRecordList(params) {
  return request({
    url: '/admin/bankrecord/',
    method: 'get',
    params
  })
}

export function exportOrderList(params) {
  return request({
    url: '/admin/export_orders/',
    method: 'get',
    params
  })
}

export function getOrderDetail(id) {
  return request({
    url: `/admin/get_order_detail/${id}/`,
    method: 'get'
  })
}

export function matchRecord(data) {
  return request({
    url: '/admin/record_match_order/',
    method: 'post',
    data
  })
}

export function getSmsList(params) {
  return request({
    url: '/admin/sms/',
    method: 'get',
    params
  })
}

export function getOrderWithdrawList(params) {
  return request({
    url: '/admin/order_withdraw/',
    method: 'get',
    params
  })
}

export function editWithdrawOrder(data) {
  return request({
    url: `/admin/order_withdraw/${data.order_id}/`,
    method: 'post',
    data
  })
}

export function pushWithdrawOrder(data) {
  return request({
    url: `/admin/order_withdraw/${data.order_id}/`,
    method: 'put',
    data
  })
}

export function editOrder(data) {
  return request({
    url: `/admin/order/${data.id}/`,
    method: 'put',
    data
  })
}
