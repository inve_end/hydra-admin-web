import request from '@/utils/request'

export function getRoleList( params ) {
  return request( {
    url: '/admin/manage/get_role_list/',
    method: 'get',
    params
  } )
}

export function getRoleDetail( id ) {
  return request( {
    url: `/admin/manage/get_role/${id}/`,
    method: 'get'
  } )
}

export function getPermTree() {
  return request( {
    url: '/admin/manage/get_perm_tree/',
    method: 'get'
  } )
}


export function saveRoleInfo( data, id ) {
  return request( {
    url: `/admin/manage/update_role_info/${id}/`,
    method: 'post',
    data
  } )
}

export function createRole( data ) {
  return request( {
    url: `/admin/manage/create_role/`,
    method: 'post',
    data
  } )
}

export function getPermList( params ) {
  return request( {
    url: '/admin/manage/get_perm_list/',
    method: 'get',
    params
  } )
}

export function deleteRole( id ) {
  return request( {
    url: `/admin/manage/delete_role/${id}/`,
    method: 'post'
  } )
}

export function saveUserPerm(data, id) {
  return request({
    url: `/admin/permission/${id}`,
    method: 'put',
    data
  })
}
